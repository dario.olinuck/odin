import { Component, OnInit } from '@angular/core';

//agregados

import { User } from '../Modules/user';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { FormsModule, FormBuilder, FormControl, Validators, AbstractControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import * as firebase from 'firebase';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  user = {} as User;
 
  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  usr:AbstractControl;
  public mostrar = true;

  constructor(private router: Router, private afAuth: AngularFireAuth) {
   
    this.formgroup = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6)]
      ),
      'usr': new FormControl('', [
        Validators.required,
        Validators.minLength(6)]
      )
    })
    this.email = this.formgroup.controls['email'];
    this.password = this.formgroup.controls['password'];
    this.usr = this.formgroup.controls['usr'];

    this.freno(1000).then(()=>{
      this.mostrar = false;
    });
   }

  ngOnInit() {
  
  }
  
  async registrarse(usuario: User) {

    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(usuario.correo, usuario.clave);
      console.log(usuario);
    }
    catch (e) {
      console.error(e);
    }
    this.guardarUsuario(usuario);
  }

  guardarUsuario(usuario:User) {
    
     var usuariosRef = firebase.database().ref().child("usuarios");
     usuariosRef.push({ user: usuario.user, email: usuario.correo, password: usuario.clave}).then(()=>{
      this.router.navigate(['home']);
     });
  }

  salir()
  {
    this.router.navigate(['ingreso']);
  }

  async freno(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }
}

