import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {timer} from 'rxjs'
@Component({
  selector: 'app-error-logueo',
  templateUrl: './error-logueo.page.html',
  styleUrls: ['./error-logueo.page.scss'],
})
export class ErrorLogueoPage implements OnInit {
  public mostrar = true;
  constructor(private router: Router) { 
    timer(2000).subscribe(()=>   
      this.mostrar = false
      );
  }

  ngOnInit() {
  }

  volverALogueo(){
    this.router.navigate(['ingreso']);
  }


  registrarse(){
    this.router.navigate(['registro']);
  }

}
