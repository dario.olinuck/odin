import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Router } from '@angular/router';
import {timer} from 'rxjs'


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  showSplash: boolean = true;
  
  constructor(
    private router:Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    
  }

  initializeApp() {
  
    this.platform.ready().then(() => {    
      this.statusBar.styleDefault();
      this.splashScreen.hide();    
      //lo justo es 3600
      timer(3600).subscribe(()=>   
      this.showSplash = false
      );
      this.router.navigateByUrl('ingreso');   
    });
  }




  async freno(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }

}
