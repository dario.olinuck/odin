import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

//=====Agregados

import {FormsModule, FormBuilder,FormControl,Validators,AbstractControl, FormGroup,ReactiveFormsModule } from '@angular/forms';


import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//audio
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { FIREBASE_CONFIG } from './app.firebase.config';


//vibracion
import { Vibration } from '@ionic-native/vibration/ngx';

//flash
import { Flashlight } from '@ionic-native/flashlight/ngx';


//AUTH

import {AngularFireAuthModule} from 'angularfire2/auth';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(), 
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AppRoutingModule,
    FormsModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
  ],
  providers: [
    NativeAudio,
    Vibration,
    Flashlight,
    StatusBar,
    SplashScreen,    
    BarcodeScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
