import { Component, OnInit } from '@angular/core';
import { User } from '../Modules/User';

import { UsuariosService } from '../Servicios/usuarios.service';


import * as firebase from 'firebase';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { ImplicitReceiver } from '@angular/compiler';

import { Router } from '@angular/router'

@Component({
  selector: 'app-test-ingresos',
  templateUrl: './test-ingresos.page.html',
  styleUrls: ['./test-ingresos.page.scss'],
})
export class TestIngresosPage implements OnInit {


  lista: Array<User> = new Array<User>();
  auxiliar: User;

  constructor(private servicioUsuarios: UsuariosService, private router: Router) {

    this.lista = this.servicioUsuarios.Usuarios();

  }

  ngOnInit() {
  }

  Ingresar(evento) {
    let opcion = evento.toElement.id;

    switch (opcion) {
      case "1":
        this.IngresoFirebase(this.lista[0]);        
        break;
      case "2":
        this.IngresoFirebase(this.lista[1]);        
        break;
      case "3":
        this.IngresoFirebase(this.lista[2]);        
        break;
      case "4":
        this.IngresoFirebase(this.lista[3]);        
        break;
      case "5":
        this.IngresoFirebase(this.lista[4]);        
        break;
      default:
        console.log("Mi burro perdió el zapato");
        break;
        
    }
    //this.router.navigateByUrl('home')
  }


  IngresoFirebase(user: User) {
    firebase.auth().signInWithEmailAndPassword(user.correo, user.clave)
      .then(this.paginaPrincipal(user.perfil)        

      ).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage);
      });
  }


  paginaPrincipal(usuarioLogueado):any
  {
    localStorage.setItem('usuario',usuarioLogueado)        
    this.router.navigate(['home']);
  }

  salir()
  {
    this.router.navigate(['ingreso']);
  }

}
